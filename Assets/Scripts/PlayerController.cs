﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed = 5;
    public Text scoreText;
    public Text winText;

    private Rigidbody rigidbody;
    private int score = 0;

	// Initialization
	void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();

        setScoreText();

        winText.text = "";
    }

    // Called once per fixed frame
    void FixedUpdate()
    {
        // Move Player
        float moveX = Input.GetAxis("Horizontal") * speed;
        float moveZ = Input.GetAxis("Vertical") * speed;

        Vector3 movement = new Vector3(moveX, 0, moveZ);

        rigidbody.AddForce( movement );
    }

    private void OnTriggerEnter(Collider other)
    {
        // On pickup touch
        if( other.gameObject.CompareTag("Pickup") )
        {
            other.gameObject.SetActive(false);
            score++;
            setScoreText();
        }
    }

    void setScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
        if (score >= 12)
            winText.text = "You Win!";
    }
}
